'use strict';

var fs = require('fs');

var config = {
  resolve: {
   modules: ['/usr/share/nodejs'],
  },

  resolveLoader: {
   modules: ['/usr/share/nodejs'],
  },

  module: {
  rules: [
    {
      test: /\.m?js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }
  ]
},
};

var commonjsConfig = Object.assign({}, config, {
   output: {
    globalObject: 'this',
    libraryTarget: 'commonjs',
    library: {
      name: 'VueResource',
      type: 'var',
    },
    libraryExport: 'default',
  },
});

var umdConfig = Object.assign({}, config, {
   output: {
    globalObject: 'this',
    libraryTarget: 'umd',
    library: {
      name: 'VueResource',
      type: 'var',
    },
    libraryExport: 'default',
  },
});

// Return Array of Configurations
module.exports = [
    commonjsConfig, umdConfig,
];
